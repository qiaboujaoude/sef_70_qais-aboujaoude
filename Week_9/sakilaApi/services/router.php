<?php
class Router
{
    public function __construct()
    {
        print_r($this->parseURL());
        $url = $this->parseURL();
    }

    public function parseURL()
    {
        if (isset($_GET['url'])) {
            return $url = explode('/', filter_var(rtrim($_GET['url'], '/'), FILTER_SANITIZE_URL));
        }
    }
}
