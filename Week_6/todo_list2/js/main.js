var ToDo = {};
(function () {

    this.createNewDivs = function (key, title, timestamp, description) { 

        // Create Parent Node
         var newList = document.createElement('div'); 
        newList.setAttribute('class', 'dynamic-div');
        newList.setAttribute('id', key);

        // Create Button
        var listButtonContainer = document.createElement('div');
        listButtonContainer.setAttribute('class', 'delete-button');
        var listButton = document.createElement('button');
        listButton.setAttribute('onclick', 'ToDo.deleteDivs(' + key +');');
        listButton.setAttribute('id', '1');
        var buttonX = document.createTextNode('X');
        listButton.appendChild(buttonX);
        listButtonContainer.appendChild(listButton);

        // Create separator
        var listSeparator = document.createElement('span');
        listSeparator.setAttribute('class', 'vertical-separator');

        // Create title
        var listHeader = document.createElement('h3'); 
        listHeader.setAttribute('id', 'headerText')
        var listHeaderContent = document.createTextNode(title);
        listHeader.appendChild(listHeaderContent);

        // Create timestamp
        var listTimestamp = document.createElement('p');
        // listTimestamp.setAttribute('id', 'timeAdded');
        var listTimestampContent = document.createTextNode('Added: ' + timestamp);
        listTimestamp.appendChild(listTimestampContent);

        // Create descreption
        var listDescription = document.createElement('h5');
        // listDescription.setAttribute('id', 'descText');
        var listDescriptionContent = document.createTextNode(description);
        listDescription.appendChild(listDescriptionContent);

        //Append children
        newList.appendChild(listButtonContainer);
        newList.appendChild(listSeparator);
        newList.appendChild(listHeader); 
        newList.appendChild(listTimestamp);
        newList.appendChild(listDescription);
        var currentDiv = document.getElementById('dom-add'); 
        todo.insertBefore(newList, currentDiv); 

    };

    this.deleteDivs = function (key) {

        var parent = document.getElementById(key);

        while (parent.firstChild) {
            parent.removeChild(parent.firstChild);
        }

        parent.parentNode.removeChild(parent);
        localStorage.removeItem(key);

    };

    addEventListener('load', function () {

        // if (localStorage.getItem('keyID') === null) {
        //     alert ('HIIII');
        // } 

        this.getLocal = function () {      

            var dataArray = [];
            var keys = Object.keys(localStorage);
            var i = keys.length;

            while (i--) {
                var data = localStorage.getItem(keys[i]);
                var obj = JSON.parse(data);
                dataArray.push(obj);
            }
            
            return dataArray;

        } 
        
        for (var i = 0; i < getLocal().length; i++) {
            ToDo.createNewDivs(getLocal()[i].keyID, getLocal()[i].title, getLocal()[i].timestamp, getLocal()[i].description);
        } 
       
    });

    document.getElementById('submitButton').addEventListener('click', function () {

        this.formatTime = function (timeinput) {

            var day = timeinput.getDay();
            var month = timeinput.getMonth();
            var daysArray = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
            var monthsArray = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 
                            'August', 'September', 'October', 'November', 'December'];

            if (timeinput.getMinutes() < 10) {
                var minute = '0' + timeinput.getMinutes();
            }  else {
                minute = timeinput.getMinutes();
            }  

            return (daysArray[day] + ', ' + monthsArray[month] + ' ' + timeinput.getDate() +  ' ' + timeinput.getFullYear()
                + ' ' + timeinput.getHours() + ':' + minute);
        }

        this.clearFields = function  () {

            document.getElementById('title').value = '';
            document.getElementById('description').value = '';

        }

        if (document.getElementById('title').value == '' || 
            document.getElementById('description').value == '') {
    
                document.getElementById('description').innerHTML = "Can't add empty values. Please try again";
                document.getElementById('title').focus();

        } else {
            var storage = {
                title: document.getElementById('title').value,
                description: document.getElementById('description').value,
                timestamp: this.formatTime(new Date()),
                keyID: Date.now(),
                store: function  () {
                    localStorage.setItem(this.keyID, JSON.stringify(storage));
                }
            }

           storage.store();
           ToDo.createNewDivs(storage.keyID, storage.title, storage.timestamp, storage.description);

        }
       
        this.clearFields();

    });

    document.getElementById('darkTheme').addEventListener('click', function () {

        document.body.style.backgroundColor = sessionStorage.getItem('bg');
        document.body.style.color = sessionStorage.getItem('cc');
        document.querySelector('button').style.color = sessionStorage.getItem('bb');

        this.darkMode = function () {

            if (sessionStorage.getItem('bg') === '#ffffff') {

                sessionStorage.setItem('bg', '#061725');
                sessionStorage.setItem('cc', '#bfbfbf');
                sessionStorage.setItem('bb', '#ffffff');

            } else if (sessionStorage.getItem('bg') == null || undefined) {

                sessionStorage.setItem('bg', '#061725');
                sessionStorage.setItem('cc', '#bfbfbf');
                sessionStorage.setItem('bb', '#ffffff');
                
            } else if( sessionStorage.getItem('bg') === '#061725') {
                
                sessionStorage.setItem('bg', '#ffffff');
                sessionStorage.setItem('cc', '#333');
                sessionStorage.setItem('bb', '#4F4F4F');
        
            }
            
        }

        document.body.style.backgroundColor = sessionStorage.getItem('bg');
        document.body.style.color = sessionStorage.getItem('cc');
        document.querySelector('button').style.color = sessionStorage.getItem('bb');
        this.darkMode();
    });

}).apply(ToDo);


