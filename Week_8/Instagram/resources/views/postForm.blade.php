@extends('layouts.app')
@section('content')
<div class="container">

    <form action="{{url('posts')}}" method="post" enctype="multipart/form-data">

    {{-- CSRF Token. ----------------------------}}
    {{ csrf_field() }}

    {{-- Errors field. ----------------------------}}

{{--    <ul class="errors">
    @foreach ($errors->all() as $message)
        <li><p>{{ $message }}</p></li>
    @endforeach
    </ul> --}}

    {{-- Picture field. ----------------------------}}
    <div>
    <label for="image">Photo</label>
    <input type="file" name="image">
    </div>
    {{-- Caption field. ----------------------------}}
    <label for="caption">Caption</label>
    <input type="text" name="caption">
    <div>
    {{-- Form submit button. --------------------}}
    <input type="submit" value="upload!" />
    </div>
</form>

</div>
@endsection
