<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{

    public function welcomePage () 
    {
        return view('slack');
    }

    public function chatPage () 
    {
        return view('chat');
    }
}
