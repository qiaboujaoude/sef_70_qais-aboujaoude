<?php 
require_once ('Config.php');

class DBConnection 
{
    protected static $connection;

    function __construct() 
    {    
        $this->_connect();
    }

    function __destruct() 
    {
        self::$connection->close();
        // echo "CONNECTION CLOSED!!!";
    }

    private function _connect() 
    {
        if (!isset(self::$connection)) {
            // $config = parse_ini_file('./config.ini'); 
            // print_r($config);
            self::$connection = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE);
            // self::$connection = new mysqli($config['localhost'],$config['username'],$config['password'],$config['dbname']);
            echo "CONNECTION WAS SUCCESSFUL";
        }

        if (self::$connection === false) {
            // return mysqli_error($connection);
            printf ("Errormessage: ", self::$connection->error);
            echo "CONNECTION ERROR";
        }
        return self::$connection;
    }

    public function query($query) 
    {            
        $connection = $this->_connect();
        $result = $connection->query($query);
        return $result;
    }

    public function select($query)
    {
        $rows = array();
        $result = $this->query($query);

        if ($result === false) {
            return false;
        }

        while ($row = $result->fetch_assoc()) {
            $rows[] = $row;
        }
        return $rows;
    }

    public function quote($value)
    {
        $connection = $this->_connect();
        //return the value inside quotes
        return "'" . $connection->real_escape_string($value) . "'";
    }     
}