<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{

    public function user()
    {
    	// return $this->belongsTo(User::class);
    	return $this->belongsTo('App\User', 'user_id');
    }

    protected $primaryKey = 'post_id';


    protected $fillable = [
        'post_title','post_content', 'user_id',
    ];

}
