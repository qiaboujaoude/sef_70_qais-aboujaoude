<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Slack!</title>
        <meta charset="UTF-8">
        <meta name="csrf-token" id="Token" content="{{ csrf_token() }}">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="{{{ asset('favicon.png') }}}">
        <link href="{{ URL::asset('css/style.css') }}" rel="stylesheet">
    </head>
    <body>
        {{-- START app-layout   --}}
        <div class='app-layout'>
            <div class='channels box'>Channels</div>
            <div class='header box'>Channel Name</div>
            {{-- START messages box   --}}
            <div class='messages box' id="messageBox">
                <div class='message-list' id='messageList'>    
                    @foreach ($messages as $message)
                        <div class="userMessage">
                        {{$message['user_id']}} said: {{$message['message']}} 
                        </div>
                    @endforeach
                    <div id="messagesAnchor"></div>
                </div>
            </div> {{-- END messages box   --}}
            <div class='input box'>
                <input type='text' id="message" placeholder='Say something!!!'>
            </div> {{-- END input box   --}}
        </div>{{-- END app-layout   --}}
        {{--  scripts   --}}
        <script src="{{ URL::asset('js/webSocket.js') }}"> </script>
    </body>
</html>
