CREATE DATABASE FinanceDB;

CREATE TABLE `FinanceDB`.`FiscalYearTable` (
                        `fiscal_year` YEAR NULL,
                        `start_date` DATE NULL,
                        `end_date` DATE NULL)
                         ENGINE = MyISAM;

SHOW DATABASES;

USE FinanceDB;

SHOW TABLES;

-- Select * from FiscalYearTable; 

-- INSERT INTO FiscalYearTable VALUES ('2001', '2003-5-12', '2005-3-12-3');


SELECT FT.fiscal_year
       FROM `FinanceDB`.`FiscalYearTable` as FT
       WHERE outside_date BETWEEN FT.start_date AND FT.end_date; 


CREATE DEFINER=`root`@`localhost` TRIGGER `FinanceDB`.`FiscalYearTable_BEFORE_INSERT` 
	BEFORE INSERT ON `FiscalYearTable` 
    FOR EACH ROW
BEGIN
	IF NEW.start_date >= NEW.end_date THEN
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'ERROR - START DATE BIGGER THAN END DATE';
    END IF;
    
--    IF (EXTRACT(MONTH FROM NEW.start_date) >= 10) THEN	SET NEW.fiscal_year = EXTRACT(YEAR FROM NEW.start_date);
--    ELSEIF (EXTRACT(MONTH FROM NEW.start_date) < 10) THEN		SET NEW.fiscal_year = EXTRACT(YEAR FROM NEW.end_date);	END IF;
END

CREATE DEFINER=`root`@`localhost` TRIGGER `FinanceDB`.`FiscalYearTable_BEFORE_UPDATE` BEFORE UPDATE ON `FiscalYearTable` FOR EACH ROW
BEGIN
IF (EXTRACT(MONTH FROM NEW.start_date) >= '10') THEN
		SET NEW.fiscal_year = EXTRACT(YEAR FROM NEW.start_date);
    ELSEIF (EXTRACT(MONTH FROM NEW.start_date) < '10') THEN
		SET NEW.fiscal_year = EXTRACT(YEAR FROM NEW.end_date);
	END IF;
END