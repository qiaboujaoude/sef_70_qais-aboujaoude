function populate () 
{
    for (var i = 0; i < getLocal().length; i++){
        var newList = document.createElement('div'); 
        newList.setAttribute('class', 'dynamic-div');
        var id = Date.now();
        newList.setAttribute('id', id);
        var listButtonContainer = document.createElement('div');
        listButtonContainer.setAttribute('class', 'delete-button');
        var listButton = document.createElement('button');
        listButton.setAttribute('onclick', 'deleteDiv(' + id +');');
        listButton.setAttribute('id', '1');
        var buttonX = document.createTextNode('X');
        listButton.appendChild(buttonX);
        listButtonContainer.appendChild(listButton);
        // create separator
        var listSeparator = document.createElement('span');
        listSeparator.setAttribute('class', 'vertical-separator');
        // create title
        var listHeader = document.createElement('h3'); 
        listHeader.setAttribute('id', 'headerText')
        var listHeaderContent = document.createTextNode(getLocal()[i].sTitle);
        listHeader.appendChild(listHeaderContent);
        // create timestamp
        var listTimestamp = document.createElement('p');
        // listTimestamp.setAttribute('id', 'timeAdded');
        var listTimestampContent = document.createTextNode('Added: ' + getLocal()[i].sTime);
        listTimestamp.appendChild(listTimestampContent);
        // create descreption
        var listDescription = document.createElement('h5');
        // listDescription.setAttribute('id', 'descText');
        var listDescriptionContent = document.createTextNode(getLocal()[i].sDesc);
        listDescription.appendChild(listDescriptionContent);

        newList.appendChild(listButtonContainer);
        newList.appendChild(listSeparator);
        newList.appendChild(listHeader); 
        newList.appendChild(listTimestamp);
        newList.appendChild(listDescription);
        var currentDiv = document.getElementById('dom-add'); 
        todo.insertBefore(newList, currentDiv); 
    }          
}

function addToList () 
{
    var title = document.getElementById('title');
    var description = document.getElementById('description');
    var button = document.getElementById('submitButton');
    var todo = document.getElementById('todo');
    var timestamp = button.value = new Date();

    createElement();
    clearFields();

    function createElement () {

  
        var newList = document.createElement('div'); 
        newList.setAttribute('class', 'dynamic-div');
        var id = Date.now();
        newList.setAttribute('id', id);

        var storage = {
            sTitle: title.value,
            sTime: formatTime(timestamp),
            sDesc: description.value, 
            sKey: id,
            store: function  () {
                localStorage.setItem(this.sKey, JSON.stringify(storage));
            },
        };
        storage.store();

            var listButtonContainer = document.createElement('div');
            listButtonContainer.setAttribute('class', 'delete-button');
            var listButton = document.createElement('button');
            listButton.setAttribute('onclick', 'deleteDiv(' + id +');');
            listButton.setAttribute('id', '1');
            var buttonX = document.createTextNode('X');
            listButton.appendChild(buttonX);
            listButtonContainer.appendChild(listButton);

            // create separator
            var listSeparator = document.createElement('span');
            listSeparator.setAttribute('class', 'vertical-separator');

            // create title
            var listHeader = document.createElement('h3'); 
            listHeader.setAttribute('id', 'headerText')
            var listHeaderContent = document.createTextNode(title.value);
            listHeader.appendChild(listHeaderContent);

            // create timestamp
            var listTimestamp = document.createElement('p');
            // listTimestamp.setAttribute('id', 'timeAdded');
            var listTimestampContent = document.createTextNode('Added: ' + formatTime(timestamp));
            listTimestamp.appendChild(listTimestampContent);

            // create descreption
            var listDescription = document.createElement('h5');
            // listDescription.setAttribute('id', 'descText');
            var listDescriptionContent = document.createTextNode(description.value);
            listDescription.appendChild(listDescriptionContent);

        newList.appendChild(listButtonContainer);
        newList.appendChild(listSeparator);
        newList.appendChild(listHeader); 
        newList.appendChild(listTimestamp);
        newList.appendChild(listDescription);
        var currentDiv = document.getElementById('dom-add'); 
        todo.insertBefore(newList, currentDiv); 
    }

    function formatTime (timeinput) {
        var day = timeinput.getDay();
        var month = timeinput.getMonth();
        var daysArray = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        var monthsArray = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 
                        'August', 'September', 'October', 'November', 'December'];

        if (timeinput.getMinutes() < 10) {
            var minute = '0' + timeinput.getMinutes();
        }  else {
            minute = timeinput.getMinutes();
        }  

        return (daysArray[day] + ', ' + monthsArray[month] + ' ' + day +  ' ' + timeinput.getFullYear()
            + ' ' + timeinput.getHours() + ':' + minute);
    }

    function clearFields () {
        title. value = '';
        description. value = '';
    }
}

function getLocal () 
{
        var dataArray = [];
        var keys = Object.keys(localStorage);
        var i = keys.length;

        while (i--) {
            var data = localStorage.getItem(keys[i]);
            var obj = JSON.parse(data);
            dataArray.push(obj);
        }
        return dataArray;
}

function deleteDiv (id) 
{
    var parent = document.getElementById(id);
    while (parent.firstChild) {
        parent.removeChild(parent.firstChild);
        }
    parent.parentNode.removeChild(parent);

    localStorage.removeItem(id);
}

document.body.style.backgroundColor = sessionStorage.getItem('bg');
document.body.style.color = sessionStorage.getItem('cc');
// document.getElementById('submitButton').style.color = sessionStorage.getItem('bb');
document.querySelector('button').style.color = sessionStorage.getItem('bb');
function darkMode() 
{
     if ( sessionStorage.getItem('bg') === '#ffffff') {
         
            sessionStorage.setItem('bg', '#061725');
            sessionStorage.setItem('cc', '#bfbfbf');
            sessionStorage.setItem('bb', '#ffffff');
            
         
     }
    else if (sessionStorage.getItem('bg') == null || undefined) {
        sessionStorage.setItem('bg', '#061725');
        sessionStorage.setItem('cc', '#bfbfbf');
        sessionStorage.setItem('bb', '#ffffff');
        
    }
    else if( sessionStorage.getItem('bg') === '#061725') {
        
        sessionStorage.setItem('bg', '#ffffff');
        sessionStorage.setItem('cc', '#333');
        sessionStorage.setItem('bb', '#4F4F4F');
        
  
    }
document.body.style.backgroundColor = sessionStorage.getItem('bg');
document.body.style.color = sessionStorage.getItem('cc');
document.querySelector('button').style.color = sessionStorage.getItem('bb');
// document.getElementById('submitButton').style.color = sessionStorage.getItem('bb');
}