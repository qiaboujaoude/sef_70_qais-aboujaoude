<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Message;
use App\User;
use Auth;
class MessageController extends Controller
{
    public function store(Request $request)
    {
        $data = $request->json()->all();
        $message = new \App\Message;
        $message->user_id = Auth::user()->id;
        $message->message = $data['message'];
        $message->save();
    }

    public function getMessages() 
    {
        $messages = Message::all();
        return view ('chat', compact('messages'));
    }
}
