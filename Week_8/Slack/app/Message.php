<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'message'
    ];

    // Message __belongs_to__ User
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
