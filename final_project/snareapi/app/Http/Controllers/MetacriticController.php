<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Goutte\Client;


class MetacriticController extends Controller
{
    function getArtist($album, $artist) 
    {
        $url = 'http://www.metacritic.com/music/' . $album . '/' . $artist; 

        $client = new Client();

        $crawler = $client->request('GET', $url);

        $output = $crawler->filter('div[class="metascore_w xlarge album positive"]')->extract('_text');

        if(empty($output)) {
            $output = $crawler->filter('div[class="metascore_w xlarge album mixed"]')->extract('_text');
        } 
        if(empty($output)) {
            $output = $crawler->filter('div[class="metascore_w xlarge album positive perfect"]')->extract('_text');
        }
        if(empty($output)) {
            $output = $crawler->filter('div[class="metascore_w xlarge album negative"]')->extract('_text');
        } 
        if (empty($output)) {
            return response()->json(['error' => 'Score not found'], 404);
        }

        $result = [
          "url"   => $url,
          "score" => $output
        ];  

        return response()->json($result);
    }

    function getArtistPicture($artist) 
    {
        $url = 'https://www.last.fm/music/' . $artist; 
        
        $client = new Client();
        
        $crawler = $client->request('GET', $url);
        
        $output = $crawler->filter('header > div')->extract('style');
        // dump($output);
        // $regex = '/\["background-image: url\((.*)\)/';
        $regex = '/url\((.*)\)/';
        preg_match($regex, $output[0], $match);

        dump($match[1]);
        
        // copy($match[1], '../../public/images/file.jpeg');

        // file_put_contents("Tmpfile.zip", fopen("http://someurl/file.zip", 'r'));
        
        // file_put_contents($img, file_get_contents($url));
        

        // return response()->json($output);        
        return '<img src=' . $match[1] . '">';
    }
}

