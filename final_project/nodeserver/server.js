const express = require('express')
const hbs = require('hbs')
// var http = require('http');
var app = express()
// var p4k = require('./p4k.js')
var pitchfork = require('pitchfork')

var cors = require('cors')
app.use(cors())

const getColors = require('get-image-colors')

app.get('color/hex/:artist/:album', (req, res) => {
  let artist = req.params.artist
  let album = req.params.album
  let imageURL = `${artist}_${album}.jpg`
  getColors(`../snare/static/albums/${imageURL}`).then(colors => {
  // getColors(`../snare/static/albums/${imageURL.replace(/%20/g, " ")}`).then(colors => {
    res.send(colors.map(color => color.hex()))
  })
})


// app.set('view enginge', 'hbs')

// app.use(express.static(__dirname + '/public'))

// app.get('/', (request, response) => {
//       response.send(p4k.p)
// }) 

app.get('/', (request, res) => {
  res.send('hi')
})


app.get('/pitchfork/:album/:artist', (req, res) => {
  let list = []
  // var search = new pitchfork.Search("radiohead kid a")  
  let album = req.params.album
  let artist = req.params.artist
  let query = album + ' ' + artist
  let search = new pitchfork.Search(query)  
  search.promise.then(results => {
    results.forEach(review => {
      list.push(review.attributes)
      res.send(list)
    })
  })
})

// app.get('/about', (request, response) => {
//   response.render('about.hbs', {
//     pageTitle: 'About Page',
//     currentYear: new Date().getFullYear()
//   })
// })

// app.get('/p4k', (request, response) => {
//   response.send()
// })

app.listen(3000)

