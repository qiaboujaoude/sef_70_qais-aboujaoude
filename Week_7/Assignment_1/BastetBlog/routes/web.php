<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
 
Route::resource('blog', 'BlogPostsController');

Route::get('/welcome', 'BlogPostsController@showPosts');

Route::get('/blog', function(){
    return view('/blog.create');
});

Route::get('/blog.create', 'BlogPostsController@create');