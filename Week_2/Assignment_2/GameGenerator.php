<?php

class GameGenerator {
    // Class Members
    private static $bigNumbers;
    private static $smallNumbers;
    private $randomNumber;
    private $numbersArray;

    // Class Constructor 
    function __construct () {
        self::$bigNumbers =   array(25, 50, 75, 100);
        self::$smallNumbers = array(1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 
                                    6, 6, 7, 7, 8, 8, 9, 9, 10, 10);
        $this->randomNumber = rand(101, 999); 
        $this->numbersArray = array();
        $this->fillArray();
    }

    // Class Destructor 
    function __destrcut () {
        // Not sure if I need one
    }

    function fillArray () {
        shuffle(self::$bigNumbers);
        shuffle(self::$smallNumbers);
        $bigSelector = rand (1, 4);
        $smallSelector = 6 - $bigSelector;

        for ($i = 1; $i <= $bigSelector; $i++) {
            array_push($this->numbersArray, self::$bigNumbers[$i-1]);
        }

        for ($j = 1; $j <= $smallSelector; $j++) {
            array_push($this->numbersArray, self::$smallNumbers[$j-1]);
        }
    }

    // Get Methods
    function getNumbersArray () {
        return $this->numbersArray;
    }

    function getRandomNumber () {
        return $this->randomNumber;
    }

    // Print and testing methods
    function printArray () {
        // echo "Big Numbers are: \n";
        // print_r(self::$bigNumbers); 
        // echo "Small Numbers are: \n";
        // print_r(self::$smallNumbers);
        // echo "Random Number is: ";
        // print($this->randomNumber) . "\n";
        // echo "Filled Array: ";
        // print_r($this->getNumbersArray()); 
    }
}
