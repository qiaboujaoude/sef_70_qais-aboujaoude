<?php

class DbConnect 
{   
    private $sqlConnection; 
    /**
    * Calls the connect function to establish a   
    * connection to the database 
    *
    */
    public function __construct() 
    {
        $this->connect();
    }

    /**
     * Established a connection to the database
     * 
     * @var associative array $config  contains configurations for the DB
     *
     * @return a successful connection
     * the result as objects   
     */
    private function connect()
    {
        if (!isset($this->sqlConnection)) {
            $config = parse_ini_file('../config/config.ini'); 
            // var_dump($config);
            $this->sqlConnection = new mysqli($config['host'], $config['username'], 
                                              $config['password'], $config['schema']);
        } 
        if (($this->sqlConnection === false) || ($this->sqlConnection->connect_error)){
            return false;
        }
        return $this->sqlConnection;
    }
    
    /** 
     * Sends a query to the database
     * 
     * @var a mysqli_result object $result  contains the executed query
     *
     * @return $result 
     * the result as objects   
     */
    public function query($query)
    {
        $result = $this->sqlConnection->query($query);
        return $result;
    }

} // END Class
