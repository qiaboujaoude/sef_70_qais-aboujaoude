<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColorToAlbums extends Migration
{
    public function up()
    {
        Schema::table('albums', function($table) {
            $table->string('album_dominant_color');
        });
    }

    public function down()
    {
        Schema::table('albums', function($table) {
            $table->dropColumn('album_dominant_color');
        });
    }
}
