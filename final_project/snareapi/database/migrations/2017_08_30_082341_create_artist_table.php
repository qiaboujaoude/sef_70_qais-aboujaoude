<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArtistTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('artists', function($table) {
            $table->increments('id');
            $table->string('artist_name');
            $table->string('artist_genre');
            $table->string('artist_origin');
            $table->string('artist_profile_picture');
            $table->string('artist_overlay_picture');
            $table->text('artist_description');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('artists');
    }
}
