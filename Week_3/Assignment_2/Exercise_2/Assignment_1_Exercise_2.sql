CREATE DATABASE HospitalRecords;

CREATE TABLE `HospitalRecords`.`AnestProcedures` (
  `proc_id` INT NOT NULL,
  `anest_name` VARCHAR(45) NULL,
  `start_time` TIME NULL,
  `end_time` TIME NULL,
  PRIMARY KEY (`proc_id`),
  UNIQUE INDEX `proc_id_UNIQUE` NOT NULL AUTO_INCREMENT (`proc_id` ASC));

-- returns overlaping procedures 
SELECT  DISTINCT m1.proc_id, m2.proc_id 
FROM    (
        SELECT  m1.proc_id AS mid1, m2.proc_id AS mid2 
        FROM    AnestProcedures m1, AnestProcedures m2
        WHERE   m2.start_time BETWEEN m1.start_time AND m1.start_time
                AND m2.proc_id <> m1.proc_id
        UNION
        SELECT  m1.proc_id, m2.proc_id
        FROM    AnestProcedures m1, AnestProcedures m2
        WHERE   m2.end_time BETWEEN m1.start_time AND m1.end_time
                AND m2.proc_id <> m1.proc_id
        ) mo, AnestProcedures m1, AnestProcedures m2
WHERE   m1.proc_id = mid1
        AND m2.proc_id = mid2