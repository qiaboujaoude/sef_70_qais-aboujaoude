var webSocket = (function () {

    var instance;

    function initialize () {
        // Connect to WebSocket
        var socket =  new WebSocket('ws://localhost:12345/chat');

        socket.onopen = function() {
            xmlRequest('GET', 'send/user', null);            
        };
    
        socket.onmessage = function(message) {
            appendMessages(message.data);
        };
        // Creates a XMLHttpRequest. If the method is get it gets the user name
        function xmlRequest(method, url, toSend) {
            var xml = new XMLHttpRequest(); 
            var token = document.querySelector('meta[name="csrf-token"]');
            xml.open(method, url, true);
            xml.setRequestHeader('X-CSRF-Token', token.content);
            if (method === 'GET') {
                xml.onload = function () {
                    userName = JSON.parse(this.responseText);
                };
            } 
            xml.send(toSend);
        }
       // Creates DOM elements that contains the message. 
        function appendMessages(message) {
            message = JSON.parse(message);
            var newMessage = document.createElement('div'); 
            newMessage.setAttribute('class', 'userMessage');
            var newMessageContent = document.createTextNode(message.user + 
                                                            ' said: ' + message.message);
            newMessage.appendChild(newMessageContent);
            document.getElementById('messageList').insertBefore(newMessage, 
                                                                document.getElementById('messagesAnchor'));
        }  
        // var userName; Not sure how its working without defning the userName outside of the function scope.                                   
        return {
            // Sends the message to the socket and saves to the database
            sendMessage: function() {
                if (document.getElementById('message').value.trim() === '') {
                    return false;
                } else {    
                var message = {
                    message: document.getElementById('message').value,
                    user: userName.name,
                };
                socket.send(JSON.stringify(message));
                // calling xmlRequest to save to DB
                xmlRequest('POST', 'send/store', JSON.stringify(message));
                document.getElementById('message').value = '';
                }
            },
        };
    } // END initialize ()

    return {
        getInstance: function() {
        return instance || (instance = initialize());
        }
    };
})();

window.onload = function () {
    // Create a connection
    var socket = webSocket.getInstance();
    // Scroll to the bottom and focus
    document.getElementById('message').focus();    
    document.getElementById('messageBox').scrollTop = document.getElementById('messageBox').scrollHeight;
    // Event Listner on enter/return key press
    document.getElementById('message').addEventListener('keypress', function(keyPress) {
        if (keyPress.keyCode === 13) {
            socket.sendMessage();
    // Scroll to the bottom and focus
            document.getElementById('messageBox').scrollTop = document.getElementById('messageBox').scrollHeight;
            document.getElementById('message').focus();        
        }
    });
};
    