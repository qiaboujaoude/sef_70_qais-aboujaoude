-- DATABASE CREATION --
CREATE SCHEMA `LegalClaims` DEFAULT CHARACTER SET utf8mb4 ;

-- TABLES CREATION --
CREATE TABLE `LegalClaims`.`Claims` (
    `claim_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `patient_name` VARCHAR(70) NULL,
    PRIMARY KEY (`claim_id`))
ENGINE = InnoDB;

CREATE TABLE `LegalClaims`.`Defendants` (
    `claim_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `defendants_name` VARCHAR(70) NULL,
    PRIMARY KEY (`claim_id`))
ENGINE = InnoDB;

CREATE TABLE `LegalClaims`.`LegalClaimsCodes` (
    `claim_status` VARCHAR(4) NOT NULL,
    `claim_status_desc` VARCHAR(50) NOT NULL,
    `claim_seq` INTEGER NOT NULL,
    CONSTRAINT `PK_CalimStatusCodes` PRIMARY KEY (`claim_status`)
);
ENGINE = InnoDB;

CREATE TABLE `LegalClaims`.`LegalEvents` ( 
    `claim_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `legal_event` INTEGER NOT NULL,
    `defendant_name` VARCHAR(25) NOT NULL,
    `claim_status` VARCHAR(4) NOT NULL,
    `change_date` DATE NOT NULL,
    CONSTRAINT `FK_Claims` FOREIGN KEY (claim_id),
        REFERENCES `Claims` (claim_id),
    CONSTRAINT `PK_LegalEvents` PRIMARY KEY (legal_event),
    CONSTRAINT `FK_LegalClaimsCodes` FOREIGN KEY (claim_status)
        REFERENCES `LegalClaimsCodes` (claim_status)
)
ENGINE = InnoDB;

--POPULATING DATA INTO TABLES -- 

INSERT INTO `LegalClaims`.`Claims` (`patient_name`) 
    VALUES 
        ('Bassem Dghaid'),
        ('Omar Breidi'),
        ('Marwan Sawwan');

INSERT INTO `LegalClaims`.`Defendants` (`defendants_name`)
    VALUES 
        ('Jean Skaff'),
        ('Elie Meouchi'),
        ('Radwan Sameh'),
        ('Joesph Eid'),
        ('Paul Syoufi'),
        ('Radwan Sameh'),
        ('Issam Awwad');

INSERT INTO LegalClaimsCodes 
    VALUES 
        ('AP', 'Awaiting review panel', 1),
        ('OR', 'Panel opinion rendered', 2),
        ('SF', 'Suit filed', 3),
        ('CL', 'Closed', 4);

INSERT INTO `LegalClaims`.`LegalEvents` (`legal_event`, `defendant_name`, `claim_status`, `change_date`) 
    VALUES
        ('', 'Jean Skaff',   'AP', '2016-01-01'),
        ('', 'Jean Skaff',   'OR', '2016-02-02'),
        ('', 'Jean Skaff',   'SF', '2016-03-01'),
        ('', 'Jean Skaff',   'CL', '2016-04-01'),
        ('', 'Radwan Sameh', 'AP', '2016-01-01'),
        ('', 'Radwan Sameh', 'OR', '2016-02-02'),
        ('', 'Radwan Sameh', 'SF', '2016-03-01'),
        ('', 'Elie Meouchi', 'AP', '2016-01-01'),
        ('', 'Elie Meouchi', 'OR', '2016-02-02'),
        ('', 'Radwan Sameh', 'AP', '2016-01-01'),
        ('', 'Radwan Sameh', 'OR', '2016-02-01'),
        ('', 'Paul Syoufi',  'AP', '2016-02-02'),
        ('', 'Issam Awwad',  'AP', '2016-01-01');

-- QUERIES --   

SELECT Claim.claim_id, Claim.patient, Status.claim_status
    FROM Claims AS Claim, ClaimStatusCodes AS Status
        WHERE Status.claim_seq
            FROM ClaimStatusCodes AS StatusCode
            WHERE StatusCode.claim_seq
                IN (SELECT MAX(ClaimStatusCode.claim_seq)
                FROM LegalEvents AS AS Events,
                ClaimStatusCodes AS ClaimStatusCode
            WHERE Events.claim_status = ClaimStatusCode.claim_status AND Events.claim_id = Claim.claim_id
            GROUP BY Events.defendant));