var stack = [];
window.addEventListener('load', function(event)
{
    hanoi(3,0,1,2);
    Draw();
});

function Draw()
{
    var context = document.getElementById("canvas").getContext("2d");
    var cW = context.canvas.width, cH = context.canvas.height;
    var base = [ {"id": "0", "x": 200, "y": 300, "w": 120, "h": 40, "clr": "#EBEBEB"},
                {"id": "1", "x": 400, "y": 300, "w": 120, "h": 40, "clr": "#EBEBEB"},
                {"id": "2", "x": 600, "y": 300, "w": 120, "h": 40, "clr": "#EBEBEB"} 
    ];
    var pole = [ {"id": "pole1", "x": 200, "y": 300, "w": 15, "h": -140, "clr": "#EBEBEB"},
                 {"id": "pole2", "x": 400, "y": 300, "w": 15, "h": -140, "clr": "#EBEBEB"},
                 {"id": "pole3", "x": 600, "y": 300, "w": 15, "h": -140, "clr": "#EBEBEB"} 
    ];
    var Discs = [ {"id": 1, "x": 200, "y": 160, "w": 30, "h": -20, "clr": "#804343"},
                  {"id": 2, "x": 200, "y": 180, "w": 40, "h": -20, "clr": "#4E4545"},
                  {"id": 3, "x": 200, "y": 200, "w": 50, "h": -20, "clr": "#5E2F2F"},
                  {"id": 4, "x": 200, "y": 220, "w": 60, "h": -20, "clr": "#541C1C"},
                  {"id": 5, "x": 200, "y": 240, "w": 70, "h": -20, "clr": "#3D1D1D"},
                  {"id": 6, "x": 200, "y": 260, "w": 80, "h": -20, "clr": "#2A0000"},
                  {"id": 7, "x": 200, "y": 280, "w": 90, "h": -20, "clr": "#360000"},
                  {"id": 8, "x": 200, "y": 300, "w": 100, "h": -20, "clr": "#730000"}
    ];
    function renderBlocks()
    {
        for(var i = 0; i < base.length; i++)
        {
            var b = base[i];
            context.fillStyle = b.clr;
            context.fillRect(b.x - (b.w /2), b.y, b.w, b.h);
        }
        for(var i = 0; i < pole.length; i++)
        {
            var p = pole[i];
            context.fillStyle = p.clr;
            context.fillRect(p.x - (p.w / 2), p.y, p.w, p.h);
        }
        for(var i = 0; i < Discs.length; i++)
        {
            var d = Discs[i];
            context.fillStyle = d.clr;
            context.fillRect(d.x - (d.w/2), d.y, d.w , d.h);
        }
    }
    function animate()
    {
        context.clearRect(0, 0, cW, cH);
        renderBlocks();
        if(!stack.length == 0) {
            var param = stack.shift();
            if (base[param[1]].x < base[param[2]].x) {
            context.fillRect(Discs[param[0]].x, Discs[param[0]].y--, Discs[param[0]].w, Discs[param[0]].h);
            }
        }
    }

    var animateInterval = setInterval(animate, 20);
    context.canvas.addEventListener('click', function(event) {
        clearInterval(animateInterval);
    });

}

var hanoi = function(disc,A,B,C) {
    if (disc > 0) {
        hanoi(disc - 1,A,C,B);
        stack.push([disc,A,C]);
        hanoi(disc - 1,B,A,C);
    }
}




