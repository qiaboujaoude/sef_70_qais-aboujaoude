<?php

require_once('../services/sql_wrapper.php');

class Model
{
    // Need to implement a way not to declare a global $modelName variable
    public function __construct()
    {
    }

    /**
     * Sends a Select * query to the database
     *
     * @return returns and Associative Array containing
     * the result as objects
     */
    static function all()
    {
        $modelName = strtolower(get_called_class());
        $list = [];
        $sql = new DbConnect();
        $result = $sql->query("SELECT * FROM {$modelName}");
        while ($row = $result->fetch_object()) {
            $list[] = $row;
        }
        return $list;
    }

    /**
    * Sends a Select * query to the database
    *
    * @param $id. The id to be searched in the query
    * the result as objects
    * @return returns an object of executed array
    */
    static function find($id)
    {
        $modelName = strtolower(get_called_class());
        $sql = new DbConnect();
        $result = $sql->query("SELECT * FROM {$modelName} WHERE {$modelName}_id={$id}");
        return $result->fetch_object();
    }
}

