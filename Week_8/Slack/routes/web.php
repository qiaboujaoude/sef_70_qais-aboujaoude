<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'PagesController@welcomePage');
Route::get('/chat', 'MessageController@getMessages');
// Route::get('/chat', 'PagesController@chatPage');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/send/user','HomeController@getUser');
Route::post('/send/store', 'MessageController@store');
// Route::get('/send/get', 'MessageController@getMessages');