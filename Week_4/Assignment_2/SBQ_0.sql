USE sakila;
SHOW tables;
DESCRIBE language;
DESCRIBE actor; 
DESCRIBE actor_info;
DESCRIBE film;
DESCRIBE film_list;
DESCRIBE film_text;
DESCRIBE film_actor; 
DESCRIBE film_category;
DESCRIBE customer;
DESCRIBE customer_list;
DESCRIBE country;
DESCRIBE store;
DESCRIBE rental;
DESCRIBE payment;
DESCRIBE store;
DESCRIBE address;
DESCRIBE category;

-- SELECT queries:
SELECT * FROM rental;

SELECT CONCAT (a.first_name, ' ', a.last_name) as Actor_Name, COUNT (b.film_id) 
       FROM actor AS a, film_actor AS b WHERE a.actor_id = b.actor_id 
       GROUP BY a.actor_id; 

-- SBQ_2
-- What are the top 3 languages for movies released in 2006?

SELECT name, COUNT(language_id) as langCount FROM language WHERE language_id IN 
       (SELECT language_id FROM film) GROUP BY name; 


-- SBQ_3
-- What are the top 3 countries from which customers are originating?

SELECT country FROM customer_list 
       GROUP BY country
       ORDER BY COUNT(country) DESC
       LIMIT 3;

-- SBQ_4
-- Find all the addresses where the second address is not empty (i.e., contains some text), and return these second addresses sorted.

SELECT * FROM address WHERE address2 <> '' AND address2 IS NOT NULL ORDER BY address2; 


-- SBQ_5
-- Return the first and last names of actors who played in a film involving a “Crocodile” and a “Shark”, along with the release year of the movie, sorted by the actors’ last names.

SELECT Actor.first_name, Actor.last_name, Film.release_year FROM actor AS Actor, film AS Film, film_actor AS FilmActor 
       WHERE Actor.actor_id = FilmActor.actor_id AND FilmActor.film_id = Film.film_id
       AND Film.description LIKE '%Crocodile%' AND Film.description LIKE '%Shark%'
       ORDER BY Actor.last_name;

-- SBQ_6
--  Find all the film categories in which there are between 55 and 65 films. Return the names of these
-- categories and the number of films per category, sorted by the number of films. If there are no categories between 55 and 65, return the highest available counts.


SELECT name FROM category AS Category WHERE COUNT(category_id) > ALL (SELECT COUNT(category_id FROM film_category AS filmCat WHERE Category.category_id <> film_category.category_id)); 

-- SBQ_7
-- Find the names (first and last) of all the actors and costumers whose first name is the same as the first name of the actor with ID 8. Do not return the actor with ID 8 himself. Note that you cannot use the name of the actor with ID 8 as a constant (only the ID). There is more than one way to solve this question, but you need to provide only one solution.

SELECT Actor.first_name, Actor.last_name, Costumer.first_name, Costumer.last_name FROM actor AS Actor, customer AS Costumer WHERE Costumer.first_name; 




-- SBQ_8
-- Get the total and average values of rentals per month per year per store.

SELECT Store.store_id, AVG(Pay.amount);


-- SBQ_9
-- Get the top 3 customers who rented the highest number of movies within a given year.


SELECT first_name, last_name FROM customer WHERE customer_id IN (SELECT customer_id FROM rental);



