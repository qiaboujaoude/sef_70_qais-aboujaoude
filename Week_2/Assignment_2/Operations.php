<?php 

interface Operations {

    function Evaluate ($x, $y);
    function Symbol ();

}

class Addition implements Operations {
    
    function Evaluate ($x, $y) {
    
        $result = $x + $y; 

        if ($result <= $x || $result <= $y) {
            return 0;
        } else {
            return $result;
        }
    }

    function Symbol () {
        return "+";
    }
}

class Substraction implements Operations {

    function Evaluate ($x, $y) {
    
        if ($x < $y) {
            return $y - $x;
        } else {
            return $x - $y; 
        } 
    }

    function Symbol () {
        return "-";
    }
}

class Multiplication implements Operations {

    function Evaluate ($x, $y) {
        
        $result = $x * $y; 

        if ($result <= $x || $result <= $y) {
            return 0;
        } else {
            return $result;
        }
    }

    function Symbol () {
        return "*";
    }
} 

class Division implements Operations {

    function Evaluate ($x, $y) {

        if ($x > $y) { //swap values in case left is less than right
            $temprorary = $x;
            $x = $y; 
            $y = $temprorary;
        }

        if ($x % $y == 0) {
            return $x / $y; 
        } else {
            return 0;
        }
    }

    function Symbol () {
        return "/";
    }
}
