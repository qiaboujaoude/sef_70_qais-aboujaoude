#!/usr/bin/php
<?php
$options = getopt ("a:e:t:d:s:m:");
shell_exec ('git log > gLog.txt'); 
$logFile = file ('/home/q/projects/SEF_70_Qais-Aboujaoude/Week_2/gLog.txt', FILE_SKIP_EMPTY_LINES); 
$logFileChunked = array_chunk ($logFile, 6);
$logCount = 0; 
$searchKey = "";
$searchFlag = "";

$log = array_map (function ($array) {
    $log = new Stdclass;
    $log->Hash = $array[0];
    $log->Author = $array[1];
    $log->Date = $array[2];
    $log->Comment = $array[4];
      
    return $log;
},  $logFileChunked);

userInput ($options, $logCount, $searchKey, $searchFlag); 
// echo "Search key is: $searchKey \nand search Flag is: $searchFlag \n"; // Comment out 

foreach ($log as $obj) {  
        $logCount++; 
        $subHash = substr ($obj->Hash, 7);
        $subAuthor = substr ($obj->Author, 8, 5); //Fix this
        $subDate = substr ($obj->Date, 8);
        $subComment = substr ($obj->Comment, 4);

        switch ($searchFlag) { 
            case 'a' : 
                if (preg_match($searchKey, $subAuthor)) {
                    echo "$logCount:: $subHash - $subAuthor - $subDate - $subComment \n";
                }
                break;
            
            case 'e' : //stopped here
                if (preg_match()){
                    echo "$logCount:: $subHash - $subAuthor - $subDate - $subComment \n";
                }
                break;

            case 't' : 
                if (preg_match()){
                    echo "$logCount:: $subHash - $subAuthor - $subDate - $subComment \n";
                }
                break;

            case 'd' : 
                if (preg_match()){
                    echo "$logCount:: $subHash - $subAuthor - $subDate - $subComment \n";
                }
                break;

            case 's' : 
                if (preg_match()){
                    echo "$logCount:: $subHash - $subAuthor - $subDate - $subComment \n";
                }
                break;

            case 'm' : 
                if (preg_match()){
                    echo "$logCount:: $subHash - $subAuthor - $subDate - $subComment \n";
                }   
                break;
        } 
} 

function userInput ($input, $count, &$searchKey, &$searchFlag) {
    $searchCat = ""; 
    foreach ($input as $key => $value) {
        switch ($key) { //Comment out all the echos
            case 'a' : 
                echo "AUTHOR: $value \n"; 
                $searchKey  = '['. $value .']'; 
                $searchCat = 'Author';
                $searchFlag = 'a';
                break; 

            case 'e' : 
                echo "EMAIL: $value \n";
                $searchKey = $value; // Needs restructure in the array map
                $searchCat = 'Email';
                $searchFlag = 'e';
                break;

            case 't' : 
                echo "TIME: $value \n";
                $searchKey = $value; //([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9] 
                $searchCat = 'Time';
                $searchFlag = 't';
                break; 

            case 'd' : 
                echo "DATE: $value \n";
                $searchKey = $value;                            
                $searchCat = 'Date';
                $searchFlag = 'd';
                break;

            case 's' : 
                echo "TIMESTAMP: $value \n";
                $searchKey = $value; 
                $searchCat = 'Timestamp';
                $searchFlag = 's';
                break;
                    
            case 'm' : 
                echo "COMMIT: $value \n";
                $searchKey = $value; 
                $searchCat = 'Commit Message';
                $searchFlag = 'm';
                break;

            default: 
                $searchCat = "None";
                break;
        }
    } 
    echo "Search result by $searchCat (\"$searchKey" . "\") Total: $count \n"; //count doesn't work
}
