const mysql      = require('mysql');
const connection = mysql.createConnection({
  host     : 'sef-70-db.csl14mky3k5o.us-west-2.rds.amazonaws.com',
  user     : 'snare',
  password : 'BIR29SPV',
  database : 'snare'
});
const express = require('express')
const cors = require('cors')
const download = require('image-downloader')
const axios = require('axios')
const app = express()
const lastfmKey = '04bcaf9ea18a47f83b06b975469dd393'
const musikki = {
  headers: { 
    appkey: '431d066d9df496d9a9f6806ed7d32a0e',
    appid: '27ebcfe546cb8b114bbea7ecfc0634ba' 
  }
}
app.use(cors())


const getArtistAlbums = artist => {
    axios.get(`https://music-api.musikki.com/v1/artists?q=[artist-name:${artist}]`, musikki)
      .then(response => { 
        return axios.get(`https://music-api.musikki.com/v1/artists/${response.data.results[0].mkid}/releases?q=[release-type:Album],[release-subtype:Studio]`, musikki)
          .then(response => {
            for (let i = 0; i < response.data.results.length; i++) {
              console.log(response.data.results[i].date.year)
              let albums = response.data.results[i].title
              let year = response.data.results[i].date.year
              connection.query(`INSERT INTO albums (artist_id, album_name, album_year) VALUES ((SELECT id from artists where artist_name = '${artist}'), '${albums.replace(/'/g, ' ')}', ${year})`, (err, result) => {
                if (err) throw err;
                console.log(`Changed ${result.changedRows} row(s)`);
                }) 
            }
          })
          .catch(error => {
            console.log(error)
          })
        })//http 2
      .catch(error => {
        console.log(error)
      }) //http 1
}

const fillAlbumCover = artist => {
  axios.get(`https://music-api.musikki.com/v1/artists?q=[artist-name:${artist}]`, musikki)
      .then(response => { 
        return axios.get(`https://music-api.musikki.com/v1/artists/${response.data.results[0].mkid}/releases?q=[release-type:Album],[release-subtype:Studio]`, musikki)
          .then(response => {
            for (let i = 0; i < response.data.results.length; i++) {
              console.log(response.data.results[i].date.year)
              let albums = response.data.results[i].title
              let pictureURL = `../../static/albums/${artist.toLowerCase()}_${albums.toLowerCase()}.jpg`
              connection.query(`UPDATE albums SET album_cover= "${pictureURL}" WHERE album_name = "${albums}"`, (err, result) => {
                if (err) throw err;
                console.log(`Changed ${result.changedRows} row(s)`);
              }) 
            }
          })
          .catch(error => {
            console.log(error)
          })
        })//http 2
      .catch(error => {
        console.log(error)
      }) //http 1
}

const getAlbumDescreption = artist => {
   axios.get(`https://music-api.musikki.com/v1/artists?q=[artist-name:${artist}]`, musikki)
      .then(response => { 
        return axios.get(`https://music-api.musikki.com/v1/artists/${response.data.results[0].mkid}/releases?q=[release-type:Album],[release-subtype:Studio]`, musikki)
          .then(response => {
            for (let i = 0; i < response.data.results.length; i++) {
              let albums = response.data.results[i].title
                axios.get('http://ws.audioscrobbler.com/2.0/?method=album.getinfo&artist=' +
                          artist + '&album=' + albums +
                          '&api_key=' + lastfmKey + '&format=json')
                .then(response => {
                  let desc = response.data.album.wiki.summary + "\n" + response.data.album.wiki.content
                  let regex = new RegExp("<a[^>]*>([^<]+)<\/a>", "g")
                  desc.replace(regex, '')
                  console.log(response.data.album.wiki.summary)
                  console.log(response.data.album.wiki.content)
                  connection.query(`UPDATE albums SET album_description= "${desc.replace(/["']/g, "")}" WHERE album_name = "${albums}"`, (err, result) => {
                  if (err) throw err;
                    console.log(`Changed ${result.changedRows} row(s)`);
                  }) 
                })
                .catch(error => {
                  console.log(error)
                })
            }
          })
          .catch(error => {
            console.log(error)
          })
        })//http 2
      .catch(error => {
        console.log(error)
      }) //http 1
}



const artistList = [
      'Radiohead',
      'Deafheaven',
      'Burial',
      'black Flag',
      'Cibo Matto',
      'Megadeth',
      'La Dispute',
      'Kyuss',
      'Raekwon',
      'Quasimoto',
      'Neurosis',
      'Gorillaz',
      'Slowdive',
      'Shlohmo',
      'Metallica'
     ]


// artistList.map(artist => {
//   return getArtistAlbums(artist)
// })
artistList.map(artist => {
  return getAlbumDescreption(artist)
})
// artistList.map(artist => {
//   return fillAlbumCover(artist)
// })


app.listen(8000)
