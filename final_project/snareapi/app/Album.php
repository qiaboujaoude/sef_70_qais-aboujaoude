<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Album extends Model
{
    public $timestamps = false;

    public function artist() 
    {
        return $this->belongsTo('App\Artist', 'artist_id');
    }
}
