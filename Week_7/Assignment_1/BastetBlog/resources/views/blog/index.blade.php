
@extends('layouts.app')
@section('content')
  <div class="container">
    <table class="table table-striped">
    <thead>
      <tr>
        <th>ID</th>
        <th>Title</th>
        <th>Post</th>
        <th colspan="2">Action</th>
      </tr>
    </thead>
    <tbody>
      @foreach($posts as $post)
      <tr>
        <td>{{$post['post_id']}}</td>
        <td>{{$post['post_title']}}</td>
        <td>{{$post['post_content']}}</td>
        <td><a href="{{action('BlogPostsController@edit', $post['post_id'])}}" class="btn btn-warning">Edit</a></td>
        <td>
          <form action="{{action('BlogPostsController@destroy', $post['post_id'])}}" method="post">
            {{csrf_field()}}
            <input name="_method" type="hidden" value="DELETE">
            <button class="btn btn-danger" type="submit">Delete</button>
          </form>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
  </div>
@endsection
