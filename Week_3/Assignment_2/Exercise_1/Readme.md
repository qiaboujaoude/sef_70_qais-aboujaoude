Using MySQL create a new database named FinanceDB . This database will contain only 1 table named FiscalYearTable . This table will only store date ranges for determining what fiscal year any given
date belongs to. For example, the federal government runs its fiscal year from October 1 until the end of September.
